package com.app.strajky.filemanager;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.strajky.filemanager.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class FileAdapter extends ArrayAdapter<ListItem> {

    private Context con;
    private int id;
    private List<ListItem> items;
    private final String LOG_TAG = "FileAdapter";
    private static final boolean DEBUG = false;
    private HashMap<Integer, Boolean> mSelection = new HashMap<Integer, Boolean>();
    private List<String> selectedItemPath = new ArrayList<String>();
    private List<ListItem> deleteItems = new ArrayList<ListItem>();

    public FileAdapter(Context context, int id, List<ListItem> items) {
        super(context, id, items);

        this.con = context;
        this.id = id;
        this.items = items;
    }

    public ListItem getItem(int position) {
        return items.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater view = (LayoutInflater) con.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = view.inflate(id, null);
        }

        ListItem item = getItem(position);

        if (item != null) {
            TextView itemName = (TextView) convertView.findViewById(R.id.itemname);
            ImageView imageView = (ImageView) convertView.findViewById(R.id.itemtype);

            if (itemName != null)
                itemName.setText(item.getName());

            if (imageView != null) {
                if (item.getType().equalsIgnoreCase("folder")) {
                    imageView.setImageResource(R.drawable.ic_folder);
                } else if (item.getType().equalsIgnoreCase("file")) {
                    imageView.setImageResource(R.drawable.ic_insert_drive_file);
                } else {
                    imageView.setImageResource(R.drawable.ic_folder_open);
                }
            }

            if (mSelection.get(position) != null) {
                convertView.setBackgroundColor(getContext().getResources().getColor(R.color.colorAccent));
                itemName.setTextColor(getContext().getResources().getColorStateList(R.color.white));
            } else {
                convertView.setBackgroundColor(getContext().getResources().getColor(R.color.white));
                itemName.setTextColor(getContext().getResources().getColorStateList(R.color.black));
            }
        }

        return convertView;
    }

    public void setNewSelection(int position, boolean value) {
        mSelection.put(position, value);
        notifyDataSetChanged();
    }

    public boolean isPositionChecked(int position) {
        Boolean result = mSelection.get(position);
        return result == null ? false : result;
    }

    public Set<Integer> getCurrentCheckedPosition() {
        return mSelection.keySet();
    }

    public void removeSelection(int position) {
        mSelection.remove(position);
        notifyDataSetChanged();
    }

    public void clearSelection() {
        mSelection = new HashMap<Integer, Boolean>();
        notifyDataSetChanged();
    }

    public List<String> getSelectedItem() {
        for (Integer key: mSelection.keySet()) {
            ListItem item = getItem(key);
            String path = item.getPath();

            selectedItemPath.add(path);
        }

        return selectedItemPath;
    }

    public void removeFromList(List<ListItem>  list, FileAdapter adapter) {

        for (Integer key: mSelection.keySet()) {
            ListItem toRemove = adapter.getItem(key);

            deleteItems.add(toRemove);
        }

        for (ListItem item: deleteItems) {
            adapter.remove(item);

            if (DEBUG)
                Log.i("removelist: ", item.getName());
        }

        notifyDataSetChanged();
    }
}