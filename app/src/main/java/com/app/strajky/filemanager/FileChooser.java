package com.app.strajky.filemanager;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.strajky.filemanager.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileChooser extends AppCompatActivity {

    private static final String LOG_TAG = "FileChooser";
    private static final boolean DEBUG = false;

    private File directory;
    private ListItem item;
    protected FileAdapter adapter;
    private Toolbar toolbar;
    private List<ListItem> dataList = new ArrayList<ListItem>();
    private List<ListItem> dataList2 = new ArrayList<ListItem>();
    private Map<String, String> mExtensionsToTypes = new HashMap<>();
    private List<String> selectedItemPath = new ArrayList<String>();
    private String actualFilePath = "";
    protected ListView listview;
    protected GridView gridview;
    protected ListItem opt = new ListItem("", "", "");
    protected ObjectAnimator anim;
    protected int firstPosition;
    protected int orientation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_manager);

        // Add toolbar to activity
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        gridview = (GridView) findViewById(R.id.gridview);
        listview = (ListView) findViewById(R.id.listview);
        if (DEBUG) {
            Log.i(LOG_TAG, "listview " + listview);
            Log.i(LOG_TAG, "gridview " + gridview);
        }

        /* Get default folder from Shared Preference */
        String filePath = getDefaults(getString(R.string.folder_path), getApplicationContext());

        if (filePath == null)
            filePath = getString(R.string.folder_path);

        directory = new File(filePath);

        // TODO testnout!!!!
        if(!directory.exists() || !directory.isDirectory()) {
            filePath = getString(R.string.folder_path);
            directory = new File(filePath);
        }

        orientation = getResources().getConfiguration().orientation;

        orientationViewAdapter(orientation);
    }

    @Override
    public void onBackPressed() {
        opt = adapter.getItem(0);

        if (opt.getName().equals("..")) {

            anim = ObjectAnimator.ofFloat(listview, View.ALPHA, 0);
            anim.setDuration(350);

            /* Animation listener */
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    directory = new File(opt.getPath());
                    getFiles(directory);

                    anim = ObjectAnimator.ofFloat(listview, View.ALPHA, 0, 1);
                    anim.setDuration(350);
                    anim.start();
                }
            });

            anim.start();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(FileChooser.this);

            builder.setMessage(getResources().getString(R.string.close_app));

            builder.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                    System.exit(0);
                }
            });

            builder.setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();
        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setContentView(R.layout.activity_file_manager);

            gridview = (GridView) findViewById(R.id.gridview);
            listview = (ListView) findViewById(R.id.listview);

            // Add toolbar to activity
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

            if (DEBUG) {
                Log.i(LOG_TAG, "listview " + listview);
                Log.i(LOG_TAG, "gridview " + gridview);
            }

            orientationViewAdapter(2);

        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            setContentView(R.layout.activity_file_manager);

            gridview = (GridView) findViewById(R.id.gridview);
            listview = (ListView) findViewById(R.id.listview);
            toolbar = (Toolbar) findViewById(R.id.toolbar);

            setSupportActionBar(toolbar);

            if (DEBUG) {
                Log.i(LOG_TAG, "listview " + listview);
                Log.i(LOG_TAG, "gridview " + gridview);
            }

            orientationViewAdapter(1);
        }
    }

    private void orientationViewAdapter(int orientation) {

        // ORIENTATION_PORTRAIT
        if (orientation == 1) {

            // Get all items for view
            getFiles(directory);

            listview.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    opt = adapter.getItem(position);

                    if ((opt.getType().equalsIgnoreCase("folder")) || (opt.getType().equalsIgnoreCase("parent directory"))) {
                        anim = ObjectAnimator.ofFloat(listview, View.ALPHA, 0);
                        anim.setDuration(350);

                        /* Animation listener */
                        anim.addListener(new AnimatorListenerAdapter() {

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                directory = new File(opt.getPath());
                                getFiles(directory);

                                anim = ObjectAnimator.ofFloat(listview, View.ALPHA, 0,1);
                                anim.setDuration(350);
                                anim.start();
                            }
                        });

                        anim.start();

                    } else {
                        openFile(opt);
                    }
                }
            });

            listview.setOnItemLongClickListener(new OnItemLongClickListener() {

                @Override
                public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                    listview.setItemChecked(position, !adapter.isPositionChecked(position));

                    return false;
                }
            });

            listview.setMultiChoiceModeListener(new MultiChoiceModeListener() {
                private int selected = 0;

                public boolean onPrepareActionMode(android.view.ActionMode mode, Menu menu) {
                    return false;
                }

                public void onDestroyActionMode(android.view.ActionMode mode) {
                    adapter.clearSelection();
                }

                public boolean onCreateActionMode(android.view.ActionMode mode, Menu menu) {
                    selected = 0;

                    MenuInflater inflater = getMenuInflater();
                    inflater.inflate(R.menu.contexual_menu, menu);

                    return true;
                }

                public boolean onActionItemClicked(android.view.ActionMode mode, MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.item_delete:
                            selectedItemPath = adapter.getSelectedItem();
                            deleteFile();   // delete all selected files/folders

                            selected = 0;

                            firstPosition = listview.getFirstVisiblePosition();     // Get scrollbar position

                            adapter.removeFromList(dataList, adapter);      // Remove items from view
                            listview.setSelection(firstPosition);           // Set scrollbar position
                            adapter.clearSelection();                       // Clear selection
                            mode.finish();
                    }

                    return false;
                }

                public void onItemCheckedStateChanged(android.view.ActionMode mode, int position, long id, boolean checked) {
                    if (checked) {
                        selected++;
                        adapter.setNewSelection(position, checked);
                    } else {
                        selected--;
                        adapter.removeSelection(position);
                    }

                    mode.setTitle(selected + " selected");
                }
            });

        // ORIENTATION_LANDSCAPE
        } else {

            // Get all items for view
            getFiles(directory);

            gridview.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE_MODAL);

            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    opt = adapter.getItem(position);

                    if ((opt.getType().equalsIgnoreCase("folder")) || (opt.getType().equalsIgnoreCase("parent directory"))) {
                        anim = ObjectAnimator.ofFloat(gridview, View.ALPHA, 0);
                        anim.setDuration(450);

                        /* Animation listener */
                        anim.addListener(new AnimatorListenerAdapter() {

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                directory = new File(opt.getPath());
                                getFiles(directory);

                                anim = ObjectAnimator.ofFloat(gridview, View.ALPHA, 0,1);
                                anim.setDuration(450);
                                anim.start();
                            }
                        });

                        anim.start();

                    } else {
                        openFile(opt);
                    }
                }
            });

            gridview.setOnItemLongClickListener(new OnItemLongClickListener() {

                @Override
                public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                    gridview.setItemChecked(position, !adapter.isPositionChecked(position));

                    return false;
                }
            });

            gridview.setMultiChoiceModeListener(new MultiChoiceModeListener() {
                private int selected = 0;

                public boolean onPrepareActionMode(android.view.ActionMode mode, Menu menu) {
                    return false;
                }

                public void onDestroyActionMode(android.view.ActionMode mode) {
                    adapter.clearSelection();
                }

                public boolean onCreateActionMode(android.view.ActionMode mode, Menu menu) {
                    selected = 0;

                    MenuInflater inflater = getMenuInflater();
                    inflater.inflate(R.menu.contexual_menu, menu);

                    return true;
                }

                public boolean onActionItemClicked(android.view.ActionMode mode, MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.item_delete:
                            selectedItemPath = adapter.getSelectedItem();
                            deleteFile();   // delete all selected files/folders

                            selected = 0;

                            firstPosition = gridview.getFirstVisiblePosition();     // Get scrollbar position

                            adapter.removeFromList(dataList, adapter);      // Remove items from view
                            gridview.setSelection(firstPosition);           // Set scrollbar position
                            adapter.clearSelection();                       // Clear selection
                            mode.finish();
                    }
                    return false;
                }

                public void onItemCheckedStateChanged(android.view.ActionMode mode, int position, long id, boolean checked) {
                    if (checked) {
                        selected++;
                        adapter.setNewSelection(position, checked);
                    } else {
                        selected--;
                        adapter.removeSelection(position);
                    }

                    mode.setTitle(selected + " selected");
                }
            });

        }
    }

    private void getFiles(File file) {
        dataList.clear();

        File[] files = file.listFiles();
        actualFilePath = file.getPath();
        this.setTitle(actualFilePath);


        List<ListItem> dirList = new ArrayList<ListItem>();
        List<ListItem> fileList = new ArrayList<ListItem>();

        try {
            for (File fi : files) {
                if (fi.isDirectory()) {
                    item = new ListItem(fi.getName(), "Folder", fi.getAbsolutePath());
                    dirList.add(item);
                } else {
                    item = new ListItem(fi.getName(), "File", fi.getAbsolutePath());
                    fileList.add(item);
                }
            }
        } catch (Exception e) {

        }

        Collections.sort(dirList);
        Collections.sort(fileList);

        if (!file.getName().equalsIgnoreCase("")) {
            item = new ListItem("..", "Parent directory", file.getParent());
            dataList.add(item);
        }

        dataList.addAll(dirList);
        dataList.addAll(fileList);

        if (DEBUG)
            Log.i(LOG_TAG, "datalist: " + dataList.toString());

        // Get orientation
        orientation = getResources().getConfiguration().orientation;

        if (orientation == 1) {      // ORIENTATION_PORTRAIT
            adapter = new FileAdapter(this, R.layout.list_item, dataList);

            if (DEBUG)
                Log.i(LOG_TAG, String.valueOf(adapter));

            listview.setAdapter(adapter);
        } else {                     // ORIENTATION_LANDSCAPE
            adapter = new FileAdapter(this, R.layout.list_item_gridview, dataList);

            if (DEBUG)
                Log.i(LOG_TAG, String.valueOf(adapter));

            gridview.setAdapter(adapter);
        }

    }

    private void openFile(ListItem item) {
        File fileToOpen = new File(item.getPath());
        String mimeType = getMimeType(item.getName());
        Intent intent = new Intent(Intent.ACTION_VIEW);

        if (DEBUG)
            Log.i(LOG_TAG, "MimeType: " + mimeType);

        intent.setDataAndType(Uri.fromFile(fileToOpen), mimeType);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        if (intent.resolveActivityInfo(getPackageManager(), 0) != null) {
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), "Please install file manager", Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteFile() {
        for (String path: selectedItemPath) {
            File file = new File(path);

            if (file.isDirectory()) {
                deleteFolder(file);
            } else {
                if (file.delete()) {
                    if (DEBUG)
                        Log.i(LOG_TAG, "File " + file + " DELETED");
                } else {
                    if (DEBUG)
                        Log.i(LOG_TAG, "File " + file + " NOT DELETED");
                }
            }
        }

        Toast.makeText(getApplicationContext(), "File(s) deleted", Toast.LENGTH_SHORT).show();
    }

    private void deleteFolder(File folder) {
        if (folder.isDirectory()) {
            for (File child : folder.listFiles()) {
                deleteFolder(child);
            }
        }

        folder.delete();
    }

    /* **************************************************************************************************
     ** *************************************************************************************************
     ** This code is retrieved from this website:
     ** https://github.com/openintents/filemanager
     **
     ** Copyright (C) 2007-2014 OpenIntents.org
     ** Licensed under the Apache License, Version 2.0 (the "License");
        you may not use this file except in compliance with the License.
     ** You may obtain a copy of the License at https://www.apache.org/licenses/LICENSE-2.0
     ** Unless required by applicable law or agreed to in writing, software
        distributed under the License is distributed on an "AS IS" BASIS,
     ** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
        See the License for the specific language governing permissions and limitations under the License.
     *****************************************************************************************************/
    private static String getExtension(String uri) {
        if (uri == null) {
            return null;
        }

        int dot = uri.lastIndexOf(".");
        if (dot >= 0) {
            return uri.substring(dot);
        } else {
            // No extension.
            return "";
        }
    }

    public String getMimeType(String filename) {
        String extension = getExtension(filename);

        // Let's check the official map first. Webkit has a nice extension-to-MIME map.
        // Be sure to remove the first character from the extension, which is the "." character.
        if (extension.length() > 0) {
            String webkitMimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension.substring(1));

            if (webkitMimeType != null) {
                // Found one. Let's take it!
                return webkitMimeType;
            }
        }

        // Convert extensions to lower case letters for easier comparison
        extension = extension.toLowerCase();

        String mimetype = mExtensionsToTypes.get(extension);

        if (mimetype == null) {
            mimetype = "*/*";
        }

        return mimetype;
    }
    /* ************************************************************************************************* */
    /* ************************************************************************************************* */

    /*
     * This is for OVERFLOW MENU in right corner of display
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.toolbar_settings:
                Intent intent = new Intent(this, MyPreferenceActivity.class);
                startActivity(intent);

                return true;
            case R.id.toolbar_refresh:
                setScrollBarPosition();
                Toast.makeText(getApplicationContext(), "Refresh", Toast.LENGTH_SHORT).show();

                return true;
            case R.id.toolbar_setDefaultFolder:
                setDefaults(getString(R.string.folder_path), actualFilePath, getApplicationContext());
                Toast.makeText(getApplicationContext(), "Set default folder", Toast.LENGTH_SHORT).show();

                return true;
            default:
                return super.onOptionsItemSelected(item);

        }
    }

    public static void setDefaults(String key, String value, Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getDefaults(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getString(key, null);
    }

    private void setScrollBarPosition() {

        orientation = getResources().getConfiguration().orientation;    // Get orientation

        if (orientation == 1) {      // ORIENTATION_PORTRAIT
            firstPosition = listview.getFirstVisiblePosition();     // Get scrollbar position
            getFiles(directory);
            listview.setSelection(firstPosition);                   // Set scrollbar position
        } else {                     // ORIENTATION_LANDSCAPE
            firstPosition = gridview.getFirstVisiblePosition();     // Get scrollbar position
            getFiles(directory);
            gridview.setSelection(firstPosition);                   // Set scrollbar position
        }
    }

}
