package com.app.strajky.filemanager;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.example.strajky.filemanager.R;

public class  MyPreferenceActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction().replace(android.R.id.content, new MyPreferenceFragment()).commit();

        PreferenceManager.setDefaultValues(this, R.xml.preference_fragment, false);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        ListView list = (ListView) findViewById(android.R.id.list);
        list.setPadding(0, 0, 0, 0);
        list.setDivider(null);
    }
}