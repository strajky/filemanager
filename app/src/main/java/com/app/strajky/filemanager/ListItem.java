package com.app.strajky.filemanager;


public class ListItem implements Comparable <ListItem> {

    private String name;
    private String type;
    private String path;

    public ListItem(String name, String type, String path){
        this.name = name;
        this.type = type;
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public String getType(){
        return type;
    }

    public String getPath(){
        return path;
    }

    public int compareTo(ListItem listItem) {
        if (this.name != null)
            return this.name.toLowerCase().compareTo(listItem.getName().toLowerCase());
        else
            throw new IllegalArgumentException();
    }

    public String toString() {
        return "\n" + name + ", " + type;
    }
}
