package com.app.strajky.filemanager;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.preference.PreferenceManager;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.example.strajky.filemanager.R;

public class MyPreference extends Preference {

    public MyPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onBindView(View view) {
        super.onBindView(view);

        TextView textView = (TextView) view.findViewById(R.id.preference_summary);

        String filePath = getDefaults(getContext().getString(R.string.folder_path), getContext());

        if (filePath == null)
            filePath = getContext().getString(R.string.folder_path);

        textView.setText(filePath);
    }

    public static String getDefaults(String key, Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        return preferences.getString(key, null);
    }

}
